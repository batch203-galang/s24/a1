// console.log("Hello World");

/*
3. Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
5. Create a variable address with a value of an array containing details of an address.
6. Destructure the array and print out a message with the full address using Template Literals.
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.
9. Create an array of numbers.
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.
14. Create a git repository named S24.
15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
16. Add the link in Boodle.
*/

// Start
const message = `The cube of 2 is ${2**3}`;
console.log(message);
// End

// Start
const address = ["258 Washington Ave NW", "California 90011"];

const [address1,address2] = address;
console.log(`I live at ${address1}, ${address2}`);
// End

// Start
const animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: 1075,
	measurement: "20 ft 3 in"
}

function getAnimalDetails({name, type, weight, measurement})
{
	console.log(`${name} is a ${type}. He weighed at ${weight} kgs with a measurement of ${measurement}.`);
}

getAnimalDetails(animal);
// End

// Start
const numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) =>{
	console.log(`${number}`);
})
// End

// Start
let i = 0;
let reduceNumber = numbers.reduce((number, i) => i= i+number);
console.log(reduceNumber);
// End

// Start
class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog();

myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";

console.log(myDog);
// End